#!/usr/bin/env python3
import sys
import logging
import click
import click_log
from dotenv import load_dotenv
import psycopg2
from . import network

load_dotenv()

logger = logging.getLogger(name="resif-inv-manager")
click_log.basic_config(logger)


@click.group()
@click_log.simple_verbosity_option(logger)
@click.pass_context
def cli(ctx):
    """
    First level
    """


@cli.group(help="Manage networks")
@click.pass_context
def net(ctx):
    """
    Networks management
    """


@net.command(
    help="""Add a new network.
    Exemple:
    resif-inv-manager net add XX 2030 --restricted
    Permanent networks are automatically guessed from the FDSN code. In this case, end_year value will be ignored.
    Exemple:
    resif-inv-manager net add FR 1962
    """
)
@click.pass_context
@click.argument("net_code")
@click.argument("start_year", type=int)
@click.option(
    "--restricted",
    default=False,
    is_flag=True,
    help="Use this option for a restricted network. Default is open.",
)
def add(ctx, net_code, start_year, restricted):
    """
    Register a new network
    Add a new network in the resifInv public.networks table
    """
    # Check the parameters consistency
    net = network.Network(net_code, start_year, restricted)
    try:
        net.is_permanent()
    except ValueError:
        logger.error("Invalid network code format.")
        sys.exit(0)
    #
    # Ask for confirmation
    click.echo(f"Network: {net_code}, Start: {start_year}")
    if click.confirm("Do you want to continue?"):
        click.echo("Registering")
        # Register the network in database and notify the user.
    else:
        click.echo("Aborting")


@net.command(
    help="""Update a network's enddate or policy
    Exemple:
    resif-inv-manager net update XX 2030 --policy open
    resif-inv-manager net update XX 2030 --end 2042
    For a permanent network, just ignore the dates
    resif-inv-manager net update AA --policy open
    """
)
@click.argument("net_code")
@click.argument("start_year", type=int)
@click.option(
    "--policy",
    type=click.Choice(["open", "restricted"]),
    help="Change policy (open or restricted)",
)
@click.option("--end", type=int, help="New end year")
def update(ctx, net_code, start_year, policy, end):
    """
    Update the network identified by net_code, start_year
    """
    # Check the parameters consistency
    net = network.Network(net_code, start_year, policy == "restricted")
    try:
        net.is_permanent()
    except ValueError:
        logger.error("Invalid network code format.")
        sys.exit(0)

    # Find the registered network
    #
    # Ask for confirmation
    click.echo(f"Network: {net_code}, Start: {start_year} will be updated.")
    if click.confirm("Do you want to continue?"):
        click.echo("Registering")
        # Register the network in database and notify the user.
        # TODO
    else:
        click.echo("Aborting")


@cli.group(help="Manage permissions")
@click.pass_context
def perm(ctx):
    """
    Networks management
    """


@perm.command(
    help="""
Add a new permission for a given network.
Exemple:
resif-inv-manager perm add lagaffe XX 2030
"""
)
@click.argument("login")
@click.argument("net_code")
@click.argument("start_year", type=int)
def add(ctx, login, net_code, start_year):
    """
    Add permission
    """


@perm.command(
    help="""
Revoke a permission for a given network.
Exemple:
resif-inv-manager revoke XX 2030 lagaffe
"""
)
@click.argument("login")
@click.argument("net_code")
@click.argument("start_year", type=int)
def revoke(ctx, login, net_code, start_year):
    """
    Revoke permission
    """


if __name__ == "__main__":
    cli(ctx={})
