#!/usr/bin/env python3
import logging
import re

logger = logging.getLogger(name=__name__)


class Network:
    def __init__(self, net_code, start_year, restricted=False):
        """Initialize a network"""
        self.net_code = net_code
        self.start_year = start_year
        self.restricted = restricted

    def is_permanent(self):
        # If network code is for a permanent network, do not provide enddate.
        if re.match("^[A-W][A-Z0-9]{0,1}$", self.net_code):
            logger.info(f"Network {self.net_code} guessed as permanent network.")
            is_permanent = True
        elif re.match("^[X-Z0-9][A-Z0-9]$", self.net_code):
            logger.info(f"Network {self.net_code} guessed as temporary network.")
            is_permanent = False
        else:
            raise ValueError
        return is_permanent
